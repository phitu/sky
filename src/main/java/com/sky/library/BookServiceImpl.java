package com.sky.library;

import java.util.stream.Stream;

public class BookServiceImpl implements BookService {

    private static final String BOOK_REFERENCE_PREFIX = "BOOK-";
	private BookRepository bookRepository;
	
	public BookServiceImpl(BookRepository bookRepository) {
		this.bookRepository = bookRepository;
	}
	
	public Book retrieveBook(String bookReference) throws BookNotFoundException {
		checkBookPrefix(bookReference);
		
		Book book = findBook(bookReference);
		
		return book;
	}

	private Book findBook(String bookReference) throws BookNotFoundException {
		Book book = bookRepository.retrieveBook(bookReference);
		if(book == null)
			throw new BookNotFoundException("Cannot find book");
		return book;
	}

	private void checkBookPrefix(String bookReference) throws BookNotFoundException {
		if(!bookReference.startsWith(BOOK_REFERENCE_PREFIX))
			throw new BookNotFoundException("Book does not start with " + BOOK_REFERENCE_PREFIX + " prefix");
	}

	public String getBookSummary(String bookReference) throws BookNotFoundException {
		checkBookPrefix(bookReference);
		
		Book book = findBook(bookReference);
		
		return "[" + book.getReference() + "] " + 
			book.getTitle() + 
			" - " + 
			getReview(book.getReview());
	}

	private String getReview(String bookReview) {
		String[] bookReviewWords = bookReview.split("\\s+");
		if (bookReviewWords.length > 9) {
			String review = "";
			for(int i=0; i<9; i++) {
				review += bookReviewWords[i].replaceAll("[\\W+]", "") + " ";
			}
			return review.trim() + "...";
		}
		return bookReview;
	}
}
