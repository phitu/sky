package com.sky.library;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;

import org.junit.Before;
import org.junit.Test;

public class BookServiceImplTest {

	private static final String BOOK_REFERENCE_PREFIX = "BOOK-";

    private static final String THE_GRUFFALO_REFERENCE = BOOK_REFERENCE_PREFIX + "GRUFF472";

    private static final String THE_WIND_IN_THE_WILLOWS_REFERENCE = BOOK_REFERENCE_PREFIX + "WILL987";
    
	private BookServiceImpl bookServiceImpl;
	
	@Before
	public void setUp() {
		bookServiceImpl = new BookServiceImpl(new BookRepositoryStub());
	}
	
	@Test
	public void retrieveBookUnsuccessfulInvalidPrefix() {
		try {
			Book book = bookServiceImpl.retrieveBook("");
		} catch (BookNotFoundException e) {
			assertThat(e.getMessage(), is("Book does not start with BOOK- prefix"));
		}
	}
	
	@Test
	public void retrieveBookUnsuccessfulInvalidBook() {
		try {
			Book book = bookServiceImpl.retrieveBook("BOOK-ss");
		} catch (BookNotFoundException e) {
			assertThat(e.getMessage(), is("Cannot find book"));
		}
	}

	@Test
	public void retrieveBookFindsBook() throws BookNotFoundException {
		Book book = bookServiceImpl.retrieveBook(THE_GRUFFALO_REFERENCE);
		assertThat(book.getReference(), is(THE_GRUFFALO_REFERENCE));
		assertThat(book.getTitle(), is("The Gruffalo"));		
	}
	
	@Test
	public void retrieveBookSummary() throws BookNotFoundException {
		String bookSummary = bookServiceImpl.getBookSummary(THE_GRUFFALO_REFERENCE);
		assertThat(bookSummary, is("[BOOK-GRUFF472] The Gruffalo - A mouse taking a walk in the woods"));
	}

	
	@Test
	public void retrieveBookSummaryAppendsEllipsis() throws BookNotFoundException {
		String bookSummary = bookServiceImpl.getBookSummary(THE_WIND_IN_THE_WILLOWS_REFERENCE);
		assertThat(bookSummary, is("[BOOK-WILL987] The Wind In The Willows - With the arrival of spring and fine weather outside..."));
	}
}
